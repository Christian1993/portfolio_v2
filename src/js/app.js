import '../sass/vendors/vendors.css';
import '../sass/main.sass';

import Glide from '@glidejs/glide';
import ToggleNavbar from './components/ToggleNavbar';
import Portrait from './components/Portrait';
import {glideConfig} from './components/Glide';
import StartAnimation from './components/StartAnimation';
import Modal from './components/Modal';
import Scratch from './components/Scratch';
import CopyNumber from './components/CopyNumber';
import Anchor from './components/Anchor';

const toggleNavbar = new ToggleNavbar('.navbar', '#navbarToggle', 'move-in', 'move-out', 'hamburger--active');
const portrait = new Portrait('.portrait', 'portrait--animated');
const glide = new Glide('.glide', glideConfig).mount();
const modalAnimation = new StartAnimation(['.projects'], ['.modal']);
const modalHide = new Modal('.modal', '.modal__close', 'hidden');
const scratch = new Scratch('#scratch', 290, 200, '#670667');
const number = new CopyNumber('575270665', '.contact__copy-number', 'contact__copy-number--animated');
const anchor = new Anchor('.navbar', 'navbar__link');
