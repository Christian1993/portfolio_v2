export const glideConfig = {
  type: 'carousel',
  startAt: 0,
  perView: 3,
  breakpoints: {
    768: {
      perView: 2
    },
    576: {
      perView: 1
    }
  }
};
