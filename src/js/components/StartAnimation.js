export default class StartAnimation {
  constructor(wrappers, elements, positions = []) {
    this.wrappers = wrappers.map(wrapper => document.querySelector(wrapper));
    this.elements = elements.map(element => document.querySelector(element));
    this.positions = positions;
    this.getPositions();
    this.addEventListeners();
  }

  addEventListeners() {
    window.addEventListener('scroll', () => this.checkPosition());
  }

  getPositions() {
    if (!this.positions.length) {
      this.positions = this.wrappers.map(wrapper => {
        if (wrapper) {
          return wrapper.getBoundingClientRect().top;
        } else {
          return null;
        }
      })
    }
  }

  checkPosition() {
    const pagePosition = window.pageYOffset;
    for (let i = 0, len = this.positions.length; i < len; i++) {
      if (this.positions[i] && (this.positions[i] < pagePosition)) {
        this.elements[i].classList.add(this.wrappers[i].dataset.animation);
      }
    }
  }
}
