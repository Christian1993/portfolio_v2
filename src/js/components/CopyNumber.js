export default class CopyNumber {
  constructor(number, element, animatedClass) {
    this.number = number;
    this.element = document.querySelector(element);
    this.animatedClass = animatedClass;
    this.init();
  }

  init() {
    if (this.element) {
      this.addEventListeners();
    }
  }

  addEventListeners() {
    this.element.addEventListener('click', () => this.copy())
  }

  copy() {
    this.element.classList.remove(this.animatedClass);
    const input = document.createElement('input');
    input.style = 'position: absolute; right: -9999px; opacity: 0';
    input.value = this.number;
    document.body.appendChild(input);
    input.select();
    document.execCommand('copy');
    document.body.removeChild(input);
    this.element.classList.add(this.animatedClass);
  }
}
