export default class ToggleNavbar {
  constructor(navbar, button, navbarIn, navbarOut, buttonActive, open = false) {
    this.navbar = document.querySelector(navbar);
    this.button = document.querySelector(button);
    this.navbarIn = navbarIn;
    this.navbarOut = navbarOut;
    this.buttonActive = buttonActive;
    this.open = open;
    this.addEventListeners();
  }

  addEventListeners() {
    if (this.navbar) {
      this.button.addEventListener('click', () => this.toggle())
    }
  }

  toggle() {
    if (this.open) {
      this.navbar.classList.add(this.navbarOut);
      this.navbar.classList.remove(this.navbarIn);
      this.button.classList.remove(this.buttonActive);
    } else {
      this.navbar.classList.add(this.navbarIn);
      this.navbar.classList.remove(this.navbarOut);
      this.button.classList.add(this.buttonActive);
    }

    this.open = !this.open;
  }
}
