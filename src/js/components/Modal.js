export default class Modal {
  constructor(modal, closeBtn, hiddenClass) {
    this.modal = document.querySelector(modal);
    this.closeBtn = document.querySelector(closeBtn);
    this.hiddenClass = hiddenClass;
    this.init();
  }

  init() {
    if (this.modal) {
      this.addEventListeners();
    }
  }

  addEventListeners() {
    this.closeBtn.addEventListener('click', () => this.hide());
  }

  hide() {
    this.modal.classList.add(this.hiddenClass);
  }
}
