export default class Anchor {
  constructor(navbar, link) {
    this.navbar = document.querySelector(navbar);
    this.link = link;
    this.init();
  }

  init() {
    if (this.navbar) {
      this.addEventListeners();
    }
  }

  addEventListeners() {
    this.navbar.addEventListener('click', e => this.scroll(e))
  }

  scroll(e) {
    if (e.target.classList.contains(this.link)) {
      e.preventDefault();
      document.querySelector(e.target.dataset.target).scrollIntoView({behavior: 'smooth'});
    }
  }
}
