export default class Portrait {
  constructor(portrait, activeClass) {
    this.portrait = document.querySelector(portrait);
    this.activeClass = activeClass;
    this.init();
  }

  init() {
    if (this.portrait) {
      window.addEventListener('load', () => this.startAnimation())
    }
  }

  startAnimation() {
    this.portrait.classList.add(this.activeClass);
  }
}
