export default class Scratch {
  constructor(canvas, width, height, color, drawable = false) {
    this.canvas = document.querySelector(canvas);
    this.width = width;
    this.height = height;
    this.color = color;
    this.drawable = drawable;
    this.init();
  }

  init() {
    if (this.canvas) {
      const ctx = this.canvas.getContext('2d');
      this.setDimensions(ctx);
      this.addEventListeners(ctx);
    }
  }

  addEventListeners(ctx) {
    this.canvas.addEventListener('mousemove', e => this.draw(e, ctx));
    this.canvas.addEventListener('touchmove', e => this.drawMobile(e, ctx));
    document.querySelector('body').addEventListener('mousedown', () => this.drawable = true);
    document.querySelector('body').addEventListener('mouseup', () => this.drawable = false);
  }

  setDimensions(c) {
    this.canvas.width = this.width;
    this.canvas.height = this.height;
    c.fillStyle = this.color;
    c.fillRect(0, 0, this.width, this.height);
  }

  draw(e, c) {
    if (this.drawable) {
      const rect = this.canvas.getBoundingClientRect();
      c.clearRect(e.clientX - rect.left - 25, e.clientY - rect.top - 25, 50, 50);
    }
  }

  drawMobile(e, c) {
    const rect = this.canvas.getBoundingClientRect();
    c.clearRect(e.touches[0].clientX - rect.left - 25, e.touches[0].clientY - rect.top - 25, 50, 50);
  }

}
